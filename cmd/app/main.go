package main

import (
	db "gamebunkettgbot/pkg/database"
	"gamebunkettgbot/pkg/database/handlers"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("<h1>Hello World!</h1>"))
}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}

	DB := db.Init()
	h := handlers.New(DB)
	router := mux.NewRouter()

	router.HandleFunc("/bunker/{id}", h.GetBunker).Methods(http.MethodGet)
	router.HandleFunc("/", indexHandler)

	http.ListenAndServe(":"+port, router)
}

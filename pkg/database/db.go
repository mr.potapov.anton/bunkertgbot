package db

//https://github.com/GOLANG-NINJA/crud-app/blob/main/cmd/main.go
//https://dev.to/karanpratapsingh/connecting-to-postgresql-using-gorm-24fj

import (
	"gamebunkettgbot/pkg/database/models"
	"log"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func Init() *gorm.DB {
	dbURL := "postgres://pg:pass@localhost:5444/crud"

	db, err := gorm.Open(postgres.Open(dbURL), &gorm.Config{})

	if err != nil {
		log.Fatalln(err)
	}

	db.AutoMigrate(&models.Bunker{})

	return db
}

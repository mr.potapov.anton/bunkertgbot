package models

type Bunker struct {
	Id           int    `json:"id" gorm:"type:serial;primaryKey"`
	Descriptions string `json:"descriptions"`
	Deleted      bool   `json:"deleted"`
}
